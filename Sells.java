import java.util.Date;


public class Sells {
	
	Sells(double sellQuanity, double sellPrice, double sellNetGain, Date sellDate, double sellCommission, String sellSybol){
		this.sellQuanity = sellQuanity;
		this.sellPrice = sellPrice;
		this.sellNetGain = sellNetGain;
		this.sellDate = sellDate;
		this.sellCommission = sellCommission;
		this.setSellSybol(sellSybol);
	}
	
	private double sellQuanity;
	private double sellPrice;
	private double sellNetGain;
	private Date sellDate;
	private double sellCommission;
	private String sellSybol;

	public double getSellQuanity() {
		return sellQuanity;
	}
	public void setSellQuanity(double sellQuanity) {
		this.sellQuanity = sellQuanity;
	}
	public double getSellPrice() {
		return sellPrice;
	}
	public void setSellPrice(double sellPrice) {
		this.sellPrice = sellPrice;
	}
	public double getSellNetGain() {
		return sellNetGain;
	}
	public void setSellNetGain(double sellNetGain) {
		this.sellNetGain = sellNetGain;
	}
	public Date getSellDate() {
		return sellDate;
	}
	public void setSellDate(Date sellDate) {
		this.sellDate = sellDate;
	}
	public double getSellCommission() {
		return sellCommission;
	}
	public void setSellCommission(double sellCommission) {
		this.sellCommission = sellCommission;
	}
	public String getSellSybol() {
		return sellSybol;
	}
	public void setSellSybol(String sellSybol) {
		this.sellSybol = sellSybol;
	}
}
