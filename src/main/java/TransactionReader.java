import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
//import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
//import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import javax.swing.JOptionPane;
import javax.swing.JFileChooser;
import javax.swing.JDialog;
import java.util.Formatter;
import java.net.*;
import java.io.*;


public class TransactionReader {

	Transaction[] transactionReader(File txn) throws IOException, ParseException {//reads a transaction file & creates a txn[] for it

		FileReader fr = new FileReader(txn);
		BufferedReader br = new BufferedReader(fr);
		//		final String EF = "***END OF FILE***";
		String line = br.readLine();
		//		double lgth = txn.length();
		String [] lineList = line.split(",");
		//		int lgth = lineList.length;
		//		System.out.println(lgth);
		Transaction [] txns = new Transaction[20];


		for (int i = 0; i < 20; i++) {
			line = br.readLine();
			if(line.charAt(0) == '*'){
				//System.out.println("Broke");
				break;
			}
			//			System.out.println(line);
			lineList = line.split(",");
			//			System.out.println(Arrays.asList(lineList));
			txns[i] = new Transaction();
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			txns[i].setDt(sdf.parse(lineList[0]));
			//			System.out.println(txns[i].getDt());
			txns[i].setQty(Integer.parseInt(lineList[3]));
			txns[i].setSybmol(lineList[4]);
			txns[i].setPrice(Double.parseDouble(lineList[5]));
			if(lineList[6].trim().equals("")) {
				lineList[6] = "0";
			}
			txns[i].setCommission(Double.parseDouble(lineList[6]));
			txns[i].setbOrS(lineList[2].trim().charAt(0));
			//			System.out.println(txns[i].getbOrS());
			txns[i].setCost(txns[i].getQty()*txns[i].getPrice()+txns[i].getCommission()); 

		}
//		System.out.println("IT WORKED");
		return txns;

	}

	void positionShow_er(Transaction [] txn) {//prints out all the information for each purchase or sell
		double total = 0;
 
		for(int i = 0; i < txn.length; i++) {
			if(txn[i] == null) {
				break;
			}
			char bs = txn[i].getbOrS();
			if(bs == 'B') {
				System.out.printf("Bought %d of %s at %.2f on %tD, with %.2f commission. The total cost is %.2f.\n", txn[i].getQty(), txn[i].getSybmol(), txn[i].getPrice(), txn[i].getDt(), txn[i].getCommission(), txn[i].getCost());
				total = total - txn[i].getCost();
			}
			if(bs == 'S') {
				System.out.printf("Sold %d of %s at %.2f on %tD, with %.2f commission. The total proccedes %.2f\n", txn[i].getQty(), txn[i].getSybmol(), txn[i].getPrice(), txn[i].getDt(), txn[i].getCommission(),txn[i].getCost());
				total = total + txn[i].getCost();

			}
			System.out.println("-------------------------------------------");

		}
		System.out.println("The total is: " + total);
		System.out.println("-------------------------------------------");
		System.out.println("-------------------------------------------");
	}
	
	void returnShow_er(Transaction [] txn) {//gets user input for the current price to show % return
		Scanner s = new Scanner(System.in);
		for(int i = 0; i < txn.length; i++) {
			if(txn[i] == null) {
				break;
			}
			char bs = txn[i].getbOrS();
			if(bs == 'B') {
				System.out.println("Enter current Price for " + txn[i].getSybmol() + ":");
				txn[i].setCrntprice(s.nextDouble());
				txn[i].setRtn(((txn[i].getCrntprice()-txn[i].getPrice())/txn[i].getPrice())*100);

				//				Need to create new variable in Transaction Class to store the current price in
			}
		}
		for(int i = 0; i < txn.length; i++) {
			if(txn[i] == null) {
				break;
			}
			char bs = txn[i].getbOrS();
			if(bs == 'B') {
				System.out.println("The % return for " + txn[i].getSybmol()+ " is " + txn[i].getRtn() +"%.");
				System.out.println("-------------------------------------------");
			}	
		}	
	}

	void doWhat() throws IOException, ParseException {
		int result = 100;
		JOptionPane.showMessageDialog(null, "In the next window select the Transaction file(.CVS format) that you would like to process" , "Transactions",JOptionPane.PLAIN_MESSAGE);
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		int resultt = fileChooser.showOpenDialog(null);
		File f = null;
		Transaction[] Txns1 = null;
		if (resultt == JFileChooser.APPROVE_OPTION) {
			// user selects a file
			f = fileChooser.getSelectedFile();
			Txns1 = transactionReader(f);
//			System.out.println("A FILE WAS SELECTED");
//			System.out.println(Txns1[0].getSybmol());
		}
		while(result != JOptionPane.CLOSED_OPTION){
//			System.out.println("INitizied");
			{Object[] options1 = {"Cancel", "Show Positions", "Get the %Return"};
			result = JOptionPane.showOptionDialog(null,
					"Select What to Do",
					"Do What?",
					JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.PLAIN_MESSAGE,
					null,
					options1,
					null);
			if(result == JOptionPane.YES_OPTION){
//				System.out.println("Cancel");
				break;
				
			}
			if(result == JOptionPane.NO_OPTION){
//				System.out.println("Show Positions");
//				positionShow_er(Txns1);
				guiPositionShow(Txns1);
			}
			if(result == JOptionPane.CANCEL_OPTION){
//				System.out.println("% Return");
//				returnShow_er(Txns1);
				guiReturnShow(Txns1);
			}

			}
		}

	}
	
	void guiPositionShow(Transaction [] txn) {
		String pos = "";
		double total = 0;
		for(int i = 0; i < txn.length; i++) {
			if(txn[i] == null) {
				break;
			}
			char bs = txn[i].getbOrS();
			if(bs == 'B') {
				
				pos += String.format("Bought %d of %s at %.2f on %tD, with %.2f commission. The total cost is %.2f.\n", txn[i].getQty(), txn[i].getSybmol(), txn[i].getPrice(), txn[i].getDt(), txn[i].getCommission(), txn[i].getCost());
				total = total - txn[i].getCost();
			}
			if(bs == 'S') {
				pos += String.format("Sold %d of %s at %.2f on %tD, with %.2f commission. The total proccedes %.2f\n", txn[i].getQty(), txn[i].getSybmol(), txn[i].getPrice(), txn[i].getDt(), txn[i].getCommission(),txn[i].getCost());
				total = total + txn[i].getCost();

			}
			pos += String.format("-------------------------------------------\n");

		}
		pos += String.format("The total is: " + total + "\n");
		pos += String.format("-------------------------------------------\n");
		pos += String.format("-------------------------------------------\n");
		JOptionPane.showMessageDialog(null, pos , "Transactions",JOptionPane.PLAIN_MESSAGE);
	}
	
	void guiReturnShow(Transaction [] txn) throws MalformedURLException, IOException {
//		create a string(or double) [] x, with the length of the transaction [].
//		have each argument of x be the current value of the stock
//		get input in gui for jpaneoption: might look like this "JOptionPane.showInputDialog("Enter the length of side 'a'");"
		
		List<CurrentStockPrice> CSPList = new ArrayList<CurrentStockPrice>();

		String url = "http://finance.yahoo.com/d/quotes.csv?s=";
		for(int i = 0; i < txn.length; i++) {
			if(txn[i] == null) {
				break;
			}
			char bs = txn[i].getbOrS();
			int ii = 0;
			if(bs == 'B') {
				url = url + "+" + txn[i].getSybmol();
//				System.out.println(txn[i].getSybmol());  THIS PART WORKS!!
				CSPList.add(ii, new CurrentStockPrice(txn[i].getSybmol()));
//				System.out.println(CSPList.get(ii).getSymbol());
//				System.out.println(url);
				ii++;
				
			}			
		}
//		System.out.println(CSPList.size());

		url = url + "&f=a";
//		System.out.println(url);
		
		URL stock = new URL(url);
		BufferedReader in = new BufferedReader(new InputStreamReader(stock.openStream()));
		
		
		String inputLine;
		int q = 0;
//		System.out.println(CSPList.get(0).getSymbol());
//		System.out.println(CSPList.get(1).getSymbol());
//		System.out.println(CSPList.get(2).getSymbol());
//		System.out.println(CSPList.get(3).getSymbol());
//		System.out.println("_________________");
		while((inputLine = in.readLine()) != null) {
//			System.out.println(CSPList.get(q).getSymbol());
			CSPList.add(q, new CurrentStockPrice(CSPList.get(q).getSymbol(),Double.parseDouble(inputLine)));
//			System.out.println(CSPList.get(q).getSymbol() + " " + CSPList.get(q).getCurrentPrice());
//			System.out.println(inputLine);
//			System.out.println(CSPList.get(i).getSymbol() +"\n" + CSPList.get(i).getCurrentPrice());
			q = q + 1;
//			System.out.println("DID THE i");
		}
		in.close();
		String RReturn = "";
		for(int iii = 0; iii < txn.length; iii++) { //need to make this into a gui at some point
			if(txn[iii] == null) {
//				System.out.println("Break");
				break;
			}
			char bs = txn[iii].getbOrS();
			if(bs == 'B') {
				double returnn = ((CSPList.get(iii).getCurrentPrice()-txn[iii].getPrice())/txn[iii].getPrice())*100;
				RReturn = RReturn + "The % return for " + txn[iii].getSybmol() +" is " + returnn + "%." +"\n";
				RReturn = RReturn + "-------------------------------------------" + "\n";
				
//				System.out.println("The % return for " + txn[iii].getSybmol() +" is " + returnn +"%.");
//				System.out.println("-------------------------------------------");
			}	
		}	
		JOptionPane.showMessageDialog(null, RReturn , "Returns",JOptionPane.PLAIN_MESSAGE);
	}
}