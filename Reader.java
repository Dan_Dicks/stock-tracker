import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class Reader {

	int checkSymbol(ArrayList<Stocks> Stock, String x) {
		for (int i = 0; i < Stock.size(); i++) {
			if (((Stocks) Stock.get(i)).getSymbol().equals(x)) {
				return i;
			}
		}
		return -1;
	}

	ArrayList<Sells> sells = new ArrayList<Sells>();

	void sellll(ArrayList<Stocks> stock, int c, int d, String[] lineList) throws ParseException {
		// c is the new stock line, d is the one that needs up dated.
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		// sellLocs.add(y, d);
		// System.out.println(sellLocs.get(y));
		if (lineList[6].equals("")) {
			lineList[6] = "0";
		}
		Sells x1 = new Sells(Double.parseDouble(lineList[3]), Double.parseDouble(lineList[5]),
				(Double.parseDouble(lineList[5]) * Double.parseDouble(lineList[3])) - Double.parseDouble(lineList[9])
						- Double.parseDouble(lineList[6]),
				sdf.parse(lineList[0]), Double.parseDouble(lineList[6]), lineList[4]);
		sells.add(x1);

		stock.get(d).setQuanity(stock.get(d).getQuanity() - Double.parseDouble(lineList[3]));
		stock.get(d).setNetPrice(stock.get(d).getQuanity() * stock.get(d).getPrice() + stock.get(d).getCommission());
		// System.out.println(stock.get(d).getSellDate());

	}

	ArrayList<Stocks> update(ArrayList<Stocks> Stock, int c, int d, String[] lineList) {
		// c is the new stock line, d is the one that needs up dated.
		Stocks s1 = Stock.get(d);
		for (int i1 = 0; i1 < lineList.length; i1++) {
			if (lineList[i1].trim().equals("")) {
				lineList[i1] = "0";
			}
		}
		double totShares = (Integer.parseInt(lineList[3]) + s1.getQuanity());
		double totPrice = ((s1.getPrice() * s1.getQuanity())
				+ (Double.parseDouble(lineList[5]) * Integer.parseInt(lineList[3])));
		// System.out.println(totShares);
		// System.out.println(totPrice);
		s1.setPrice(totPrice / totShares);
		s1.setQuanity(totShares);
		s1.setNetPrice(totPrice);
		s1.setCommission(s1.getCommission() + Double.parseDouble(lineList[6]));
		return Stock;
	}

	File getFile() {
		JOptionPane.showMessageDialog(null,
				"In the next window select the Transaction file(.CVS format) that you would like to process",
				"Transactions", JOptionPane.PLAIN_MESSAGE);
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		int resultt = fileChooser.showOpenDialog(null);
		File f = null;
		if (resultt == JFileChooser.APPROVE_OPTION) {
			// user selects a file
			f = fileChooser.getSelectedFile();
			return f;
		} else {
			return f;
		}
	}

	ArrayList<Stocks> stockList() throws IOException, NumberFormatException, ParseException {
		ArrayList<Stocks> stockList = new ArrayList<Stocks>();
		File file = getFile();
		if (file == null) {
			return null;
		}
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line = br.readLine();
		// System.out.println(line);
		String[] lineList = new String[13];
		lineList = line.split(",", -1);
		// System.out.println(Arrays.asList(lineList));
		for (int i = 0; i < 20; i++) {
			line = br.readLine();
			if (line.charAt(0) == '*') {
				break;
			}
			// System.out.println(line);
			lineList = line.split(",", -1);
			// System.out.println(Arrays.asList(lineList));
			// System.out.println(lineList[2].trim().charAt(0));
			if (lineList[2].trim().charAt(0) == 'S') {
				sellll(stockList, i, checkSymbol(stockList, lineList[4]), lineList);
			}
			if (checkSymbol(stockList, lineList[4]) != -1 & lineList[2].trim().charAt(0) == 'B') {
				stockList = update(stockList, i, checkSymbol(stockList, lineList[4]), lineList);
			} else {
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				for (int i1 = 0; i1 < lineList.length; i1++) {
					if (lineList[i1].trim().equals("")) {
						lineList[i1] = "0";
						// System.out.println(Arrays.asList(lineList));
					}
				}
				Stocks t1 = new Stocks(sdf.parse(lineList[0]), Integer.parseInt(lineList[3]), lineList[4],
						Double.parseDouble(lineList[5]), Double.parseDouble(lineList[6]),
						Double.parseDouble(lineList[7]), Double.parseDouble(lineList[9]),
						(lineList[2].trim().charAt(0)));
				stockList.add(t1);
			}
		}

		return stockList;
	}

	void guiPositionShow(ArrayList<Stocks> Stock) {
		if (Stock == null) {
			return;
		}
		String pos = "";
		double total = 0;
		for (int i = 0; i < Stock.size(); i++) {
			if (Stock.get(i) == null) {
				break;
			}
			char bs = Stock.get(i).getAction();
			if (bs == 'B') {

				pos += String.format("Bought %f of %s at %.2f on %s, with %.2f commission. The total cost is %.2f.\n",
						Stock.get(i).getQuanity(), Stock.get(i).getSymbol(), Stock.get(i).getPrice(),
						Stock.get(i).getDate().toString(), Stock.get(i).getCommission(), Stock.get(i).getNetPrice());
				total = total - Stock.get(i).getNetPrice();
			}
			// if (bs == 'S') {
			// pos += String
			// .format("Sold %f of %s at %.2f on %s, with %.2f commission. The
			// total proccedes %.2f\n",
			// Stock.get(i).getQuanity(), Stock.get(i)
			// .getSymbol(), Stock.get(i).getPrice(),
			// Stock.get(i).getDate().toString(), Stock.get(i)
			// .getCommission(), Stock.get(i)
			// .getNetPrice());
			// total = total + Stock.get(i).getNetPrice();
			//
			// }
			// pos += String
			// .format("-------------------------------------------\n");
			//
		}
		pos += String.format("The total is: " + total + "\n");
		pos += String.format("-------------------------------------------\n");
		pos += String.format("-------------------------------------------\n");
		JOptionPane.showMessageDialog(null, pos, "Transactions", JOptionPane.PLAIN_MESSAGE);
	}

	void guiReturnShow(ArrayList<Stocks> Stock) throws MalformedURLException, IOException {
		if (Stock == null) {
			return;
		}
		String url = "http://finance.yahoo.com/d/quotes.csv?s=";
		ArrayList<Integer> places = new ArrayList<Integer>();
		for (int i = 0; i < Stock.size(); i++) {
			if (Stock.get(i) == null) {
				break;
			}
			char bs = Stock.get(i).getAction();
			if (bs == 'B') {
				url = url + "+" + Stock.get(i).getSymbol();
				places.add(i);
			}
		}

		url = url + "&f=a";

		URL stock = new URL(url);
		BufferedReader in = new BufferedReader(new InputStreamReader(stock.openStream()));

		String inputLine;
		int q = 0;
		while ((inputLine = in.readLine()) != null) {
			int pl = places.get(q);
			Stock.get(pl).setCurrentPrice(Double.parseDouble(inputLine));
			double returnn = ((Stock.get(pl).getCurrentPrice() - Stock.get(pl).getPrice()) / Stock.get(pl).getPrice())
					* 100;
			Stock.get(pl).setRetrn(returnn);
			q = q + 1;
		}
		in.close();
		String RReturn = "";
		for (int iii = 0; iii < Stock.size(); iii++) {
			if (Stock.get(iii) == null) {
				break;
			}
			char bs = Stock.get(iii).getAction();
			if (bs == 'B') {
				RReturn = RReturn + "The % return for " + Stock.get(iii).getSymbol() + " is "
						+ Stock.get(iii).getRetrn() + "%." + "\n";
				RReturn = RReturn + "-------------------------------------------" + "\n";

			}
		}
		JOptionPane.showMessageDialog(null, RReturn, "Returns (Based on ASK)", JOptionPane.PLAIN_MESSAGE);
	}

	void guiSellShow(ArrayList<Stocks> Stock, ArrayList<Sells> sell) {
		if (Stock == null) {
			return;
		}
		String pos = "";
		double total = 0;
		// for (int i = 0; i < Stock.size(); i++) {
		int q = 0;
		while (q < sell.size()) {
			if (Stock.get(q) == null) {
				pos = pos + "No Sells";
				// System.out.println("broke");
				break;
			}
			pos += String.format("Sold %f of %s at %.2f on %s, with %.2f commission. The total gain is %.2f.\n",
					sell.get(q).getSellQuanity(), sell.get(q).getSellSybol(), sell.get(q).getSellPrice(),
					sell.get(q).getSellDate().toString(), sell.get(q).getSellCommission(),
					sell.get(q).getSellNetGain());
			total = total + sell.get(q).getSellNetGain();
			// System.out.println(sell.get(q).getSellDate()
			// .toString());
			// NOT GETING ANY INFO FOR THE OTHER SELLS.....2
			q++;
		}
		pos += String.format("-------------------------------------------\n");

		pos += String.format("The total gain is: " + total + "\n");
		pos += String.format("-------------------------------------------\n");
		pos += String.format("-------------------------------------------\n");
		JOptionPane.showMessageDialog(null, pos, "Transactions", JOptionPane.PLAIN_MESSAGE);
	}

	void Run() throws NumberFormatException, IOException, ParseException {
		ArrayList<Stocks> stockList = stockList();
		int result = 100;
		while (result != JOptionPane.CLOSED_OPTION) {
			{
				Object[] options1 = { "Show Sells", "Show Current Positions",
						"Get the % Return for Current Positions" };
				result = JOptionPane.showOptionDialog(null, "Select What to Do", "Do What?",
						JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, options1, null);
				if (result == JOptionPane.YES_OPTION) { // Cancel Button
					guiSellShow(stockList, sells);

				}
				if (result == JOptionPane.NO_OPTION) { // Show Positions
					guiPositionShow(stockList);
				}
				if (result == JOptionPane.CANCEL_OPTION) { // Show Return
					guiReturnShow(stockList);
				}

			}
		}

	}

}
