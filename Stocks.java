import java.util.Date;

public class Stocks {

	Stocks(Date date, double quanity, String symbol, double price,
			double commission, double netPrice, double sellFee, char action) {
		this.date = date;
		this.quanity = quanity;
		this.symbol = symbol;
		this.price = price;
		this.commission = commission;
		this.netPrice = netPrice;
		this.sellFee = sellFee;
		this.action = action;
	}

	private Date date;
	private double quanity;
	private String symbol;
	private double price;
	private double commission;
	private double netPrice;
	private double sellFee;
	private char action; // B = bought S = sell

	private double currentPrice;
	private double retrn; // ((currentPrice - price)/price)*100;

	public void getAll() {
		String all = "";
		all = "Date: " + date + " Quanity: " + Double.toString(quanity)
				+ " Symbol: " + symbol + " Price: " + Double.toString(price)
				+ " Commission: " + Double.toString(commission) + " NetPrice: "
				+ Double.toString(netPrice) + " SellFee: "
				+ Double.toString(sellFee) + " Action(B:bought, S:sell)"
				+ action;
		System.out.println(all);
	}


	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getQuanity() {
		return quanity;
	}

	public void setQuanity(double quanity) {
		this.quanity = quanity;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public double getNetPrice() {
		return netPrice;
	}

	public void setNetPrice(double netPrice) {
		this.netPrice = netPrice;
	}

	public double getSellFee() {
		return sellFee;
	}

	public void setSellFee(double sellFee) {
		this.sellFee = sellFee;
	}

	public char getAction() {
		return action;
	}

	public void setAction(char action) {
		this.action = action;
	}

	public double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public double getRetrn() {
		return retrn;
	}

	public void setRetrn(double retrn) {
		this.retrn = retrn;
	}


}
