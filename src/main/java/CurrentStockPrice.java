
public class CurrentStockPrice {

	private String Symbol;
	private double currentPrice;
	
	CurrentStockPrice(String Symbol, double currentPrice) {
		this.Symbol = Symbol;
		this.currentPrice = currentPrice;
	}
	
	CurrentStockPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}
	
	CurrentStockPrice(String Symbol) {
		this.Symbol = Symbol;
	}
	public String getSymbol() {
		return Symbol;
	}
	public void setSymbol(String symbol) {
		Symbol = symbol;
	}
	public double getCurrentPrice() {
		return currentPrice;
	}
	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}
}
