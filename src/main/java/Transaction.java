import java.util.Date;



public class Transaction {
	
		private Date dt;
		private int qty;
		private String sybmol;
		private double price;
		private double commission;
		private char bOrS;
		private double cost = qty*price+commission;
		private double crntprice;
		private double rtn;
		
		public char getbOrS() {
			return bOrS;
		}
		public void setbOrS(char bOrS) {
			this.bOrS = bOrS;
		}
		public Date getDt() {
			return dt;
		}
		public void setDt(Date dt) {
			this.dt = dt;
		}
		public int getQty() {
			return qty;
		}
		public void setQty(int qty) {
			this.qty = qty;
		}
		public String getSybmol() {
			return sybmol;
		}
		public void setSybmol(String sybmol) {
			this.sybmol = sybmol;
		}
		public double getPrice() {
			return price;
		}
		public void setPrice(double price) {
			this.price = price;
		}
		public double getCommission() {
			return commission;
		}
		public void setCommission(double commission) {
			this.commission = commission;
		}
		public double getCost() {
			return cost;
		}
		public void setCost(double cost) {
			this.cost = cost;
		}
		public double getRtn() {
			return rtn;
		}
		public void setRtn(double rtn) {
			this.rtn = rtn;
		}
		public double getCrntprice() {
			return crntprice;
		}
		public void setCrntprice(double crntprice) {
			this.crntprice = crntprice;
		}
		
}
